package ai.maum.api.kbsTtsAdapter.controller;

import ai.maum.api.kbsTtsAdapter.service.ScheduledService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

@Controller
public class ScheduledController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ScheduledService service;

    @Scheduled(cron = "0 1 00 * * ?")
    public void fileRemover() {
        service.fileRemover();
    }
}
